---
title: 'Simple Models'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 2
summary: Information about the models for temperature and humidity calculation
---


### Humidity

#### Allen, 1985

I was unable to find the original publication for this formula however the function is defined as such:

```python
def relative_humidity(temperature, min_temp)
    rh = np.exp((17.625 * min_temp) / (243.04 + min_temp)) / np.exp((17.625 * temperature) / (243.04 + temperature))
    return rh * 100.0
```


### Temperature

#### Cesaraccio

From Cesaraccio et al. 2001 Int.J. Biometeorol. 161-169

This is a decent estimation of temperature over a day.

Uses *Temperature Low* and *Temperature High* for the minimum and maximum values.

Doesn't support photoperiods across midnight (`where sunset < sunrise`).

```python

alpha = max_temp - min_temp
to = max_temp - 0.39 * (max_temp - min_temp)
r = max_temp - to
hx = sunset - 4.0
hp = sunrise + 24.0
b = (min_temp - to) / np.sqrt(hp - sunset)

if hx >= time > sunrise:
    t = min_temp + alpha * np.sin((time - sunrise) / (hx - sunrise) * np.pi / 2.0)
if hx < time < sunset:
    t = to + r * np.sin(np.pi / 2.0 + (time - hx) / 8.0 * np.pi)
if sunset < time <= hp:
    t = to + b * np.sqrt(time - sunset)
if time <= sunrise:
    t = to + b * np.sqrt(time + 24.0 - sunset)
```


### Sine

Uses a sine wave between *Temperature Low* and *Temperature High*, with the trough offset to half an hour after *Sunrise*, this function ignores *Photoperiod*

```python
sr_off = 5.5 - (sunrise.hour + (sunrise.minute / 60.0))
f = 2 * np.pi / 24.0

t_avg = (min_temp + max_temp) / 2.0
t_amp = (max_temp - min_temp) / 2.0

t = t_avg - t_amp * np.sin((t + sr_off) * f)
```
