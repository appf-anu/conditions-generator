---
title: 'Sources'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 6
summary: Sources, citations, acknowledgements and Git repositories for PySolarCalc, solarcalc and this website.
---

### Spokas and Forcella (2006)

Paper by Kurt Spokas and Frank Forcella:

https://pubag.nal.usda.gov/catalog/1910


### NREL tech report

This link is the info we have for this source, but it is a dead now:

https://rredc.nrel.gov/solar/pubs/spectral/model/section3.html


### Git Repo

This repo is the original source code for the python reimplementation (which this tool is based on) of SolarCalc:

https://gitlab.com/appf-anu/pysolarcalc

This repo is the repo which builds this website and the Azure Function Apps that run the backend processing:

https://gitlab.com/appf-anu/conditions-generator