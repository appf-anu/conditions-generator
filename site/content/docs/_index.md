---
title: 'Overview'
date: 2018-11-28T15:14:39+10:00
weight: 1
---

## Growth Conditions Generator

This tool is for generating growth conditions and light settings for use with PSI growth capsules, PSI Fytopanel Lights, Heliospectra Lights and Conviron Growth Chambers.

