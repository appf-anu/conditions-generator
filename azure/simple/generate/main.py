import azure.functions as func
import json
# import logging
import datetime
import io
import re

import numpy as np
from scipy.interpolate import CubicSpline
from dateutil.parser import parse as dateutil_parse
from dateutil.parser import ParserError
from openpyxl import Workbook

all_light_headers = {
    "fytopanel": [
        "coolwhite",  # "channel-1",  # white
        "blue",       # "channel-2",  # blue
        "green",      # "channel-3",  # green
        "red",        # "channel-4",  # red
        "deepred",    # "channel-5",  #  deepred
        "farred",     # "channel-6",  # far red
        "infrared",   # "channel-7",  # infrared
        "cyan",       # "channel-8"   # NA
    ],
    "capsule": [
        "coolwhite",
        "deepred",
        "farred",
    ],
    "capsule_fancy": [
        "coolwhite",
        "deepred",
        "farred",
        "royalblue",
        "blue",
        "cyan"
    ],
    "old_fytopanel": [
        "channel-1",  # white
        "channel-2",  # blue
        "channel-3",  # green
        "channel-4",  # nearest red
        "channel-5",  # near red
        "channel-6",  # far red
        "channel-7",  # infra red
        "channel-8"   # unknown, further infra red?
    ],
    "heliospectra_s7": [
        "400nm",  # channel-1",  # 400nm
        "420nm",  # channel-2",  # 420nm
        "450nm",  # channel-3",  # 450nm
        "530nm",  # channel-4",  # 530nm
        "630nm",  # channel-5",  # 630nm
        "660nm",  # channel-6",  # 660nm
        "735nm",  # channel-7"   # 735nm
    ],
    "heliospectra_s10": [
        "370nm",  # channel-1",  # 370nm
        "400nm",  # channel-2",  # 400nm
        "420nm",  # channel-3",  # 420nm
        "450nm",  # channel-4",  # 450nm
        "530nm",  # channel-5",  # 530nm
        "620nm",  # channel-6",  # 620nm
        "660nm",  # channel-7",  # 660nm
        "735nm",  # channel-8",  # 735nm
        "850nm",  # channel-9",  # 850nm
        "6500k",  # channel-10"  # 6500k
    ],
    "heliospectra_dyna": [
        "380nm",  # channel-1",  # 370nm
        "400nm",  # channel-2",  # 400nm
        "420nm",  # channel-3",  # 420nm
        "450nm",  # channel-4",  # 450nm
        "530nm",  # channel-5",  # 530nm
        "620nm",  # channel-6",  # 620nm
        "660nm",  # channel-7",  # 660nm
        "735nm",  # channel-8",  # 735nm
        "5700k",  # channel-10"  # 6500k
    ],
    "conviron": [
        "light1",
        "light2"
    ]
}

# @cachetools.func.ttl_cache(maxsize=1, ttl=24 * 60 * 60)
temp_models = {"simple", "sine", "cesaraccio", "complex"}
humidity_models = {"simple", "allen", "complex"}


def is_night(dt, sunrise=datetime.time(6), photoperiod=datetime.timedelta(hours=16)):
    """
    test if a time is not within a givent photoperiod/sunrise, handles looping past 00:00
    """
    sunset = (datetime.datetime.combine(datetime.datetime.today(), sunrise) + photoperiod).time()
    dtt = dt.time()
    if sunset > sunrise:
        return not (sunrise <= dtt < sunset)
    return not (dtt < sunset or dtt >= sunrise)


def clamp(n, minimum=0, maximum=100):
    n = max(n, minimum)
    n = min(n, maximum)
    return n


def get_excel(light_headers):
    """
    gets an in-memory excel workbook and sheet with the title timepoints
    dateteime fields (the first two fields), require an style of width: 19
    """
    wb = Workbook(write_only=True)
    ws = wb.create_sheet(title="timepoints")
    # datetime has 19 characters
    ws.column_dimensions['A'].width = 19
    ws.column_dimensions['B'].width = 19
    ws.append(["datetime", "datetime-sim", "humidity", "temperature"] + light_headers)
    return wb, ws


# regex to match different duration keys to named groups
duration_re = re.compile(r'((?P<weeks>\d+?)w)?((?P<days>\d+?)d)?((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')


def fit_temp_cesaraccio(min_temp: float = 18,
                        max_temp: float = 24,
                        sunrise: datetime.time = datetime.time(6),
                        sunset: datetime.time = None) -> CubicSpline:
    """
    fits temperature and relative humidiy for a day given the max_temp and min_temp
    also needs sunset and sunrise
    From Cesaraccio et al. 2001 Int.J. Biometeorol. 161-169

    :param min_temp: minimum temperature for the day
    :param max_temp: maximum temperature for the day
    :param sunset: sunset time
    :param sunrise: sunrise time
    :return: spline over 24hr, temperature values
    :rtype: BSpline
    """
    if sunset is None:
        sunset = datetime.datetime.combine(datetime.datetime.min, sunrise) + datetime.timedelta(hours=16)

    sunrise = sunrise.hour + (sunrise.minute / 60.0) + (sunrise.second / 3600)

    sunset = sunset.hour + (sunset.minute / 60.0) + (sunset.second / 3600)

    assert sunrise < sunset, "this function requires that sunset be after sunrise on the same day"

    alpha = max_temp - min_temp
    to = max_temp - 0.39 * (max_temp - min_temp)
    r = max_temp - to

    hx = sunset - 4.0
    hp = sunrise + 24.0
    b = (min_temp - to) / np.sqrt(hp - sunset)

    def f(st):
        if hx >= st > sunrise:
            t = min_temp + alpha * np.sin((st - sunrise) / (hx - sunrise) * np.pi / 2.0)
        if hx < st < sunset:
            t = to + r * np.sin(np.pi / 2.0 + (st - hx) / 8.0 * np.pi)
        if sunset < st <= hp:
            t = to + b * np.sqrt(st - sunset)
        if st <= sunrise:
            t = to + b * np.sqrt(st + 24.0 - sunset)
        return t

    times = np.linspace(0, 24, 1440)
    temps = np.vectorize(f)(times)

    if temps[0] != temps[-1]:
        temps[-1] = temps[0]
    return CubicSpline(times, temps, bc_type='periodic')


def fit_temp_sine(min_temp: float = 18,
                  max_temp: float = 24,
                  sunrise: datetime.time = datetime.time(6)) -> CubicSpline:
    """
    does the same as fit_temp_rh, but uses a sine wave rather than the solar days.

    :param min_temp: minimum temperature for the day
    :param max_temp: maximum temperature for the day
    :return:
    """

    sr_off = 5.5 - (sunrise.hour + (sunrise.minute / 60.0))
    f = 2 * np.pi / 24.0

    t_avg = (min_temp + max_temp) / 2.0
    t_amp = (max_temp - min_temp) / 2.0

    times = np.linspace(0, 24, 1440)
    temps = np.vectorize(lambda t: t_avg - t_amp * np.sin((t + sr_off) * f))(times)
    return CubicSpline(times, temps, bc_type='periodic')


def fit_temp_sine_fast(min_temp: float = 18,
                       max_temp: float = 24,
                       sunrise: datetime.time = datetime.time(6)):
    """
    does the same as fit_temp_sine, but doesnt use a CubicSpline along some values
    returns a function.

    :param min_temp: minimum temperature for the day
    :param max_temp: maximum temperature for the day
    :return:
    """

    sr_off = 5.5 - (sunrise.hour + (sunrise.minute / 60.0))
    f = 2 * np.pi / 24.0

    t_avg = (min_temp + max_temp) / 2.0
    t_amp = (max_temp - min_temp) / 2.0

    def w(t):
        return t_avg - t_amp * np.sin((t + sr_off) * f)
    return w


def relative_humidity(mintemp: float, temp: float) -> float:
    """
    calculates saturated water vapor concentration with a minimum temperature for the day and temperature.
    from FAO Allen, 1985 ??

    :param mintemp: minumum temperature (C) for the day
    :param temp: current temperature (C)
    :return: estimated relative humidity
    :rtype: float
    """

    # # these values shouldnt be under 0
    # eo = max(0.6108 * np.exp((17.27 * temp) / (temp + 273.3)), 0.0)
    #
    # eos = max(0.6108 * np.exp((17.27 * mintemp) / (mintemp + 273.3)), 0.0)
    # # rh shouldnt be over 1.0
    # rh = min(eos / eo, 1.0)
    rh = np.exp((17.625 * mintemp) / (243.04 + mintemp)) / np.exp((17.625 * temp) / (243.04 + temp))
    return rh * 100.0


def parse_duration(time_str):
    """
    parse a duration string into a timedelta using duration_re
    """
    parts = duration_re.match(time_str)
    assert parts is not None, f"could not parse duration from '{time_str}'. examples of valid duration: '8h', '2d8h5m20s', '2m4s'"

    time_params = {name: int(param) for name, param in parts.groupdict().items() if param}
    return datetime.timedelta(**time_params)


def main(req: func.HttpRequest) -> func.HttpResponse:
    try:
        params = req.params.copy()

        if 'application/json' in req.headers.get('Content-Type', req.headers.get('content-type', "")):
            params.update(req.get_json())

        if req.form is not None and len(req.form):
            params.update(req.form)

        params = {k: v for k, v in params.items() if v != ""}

        start = datetime.datetime.now().replace(
            microsecond=0,
            second=0,
            minute=0,
            hour=0
        )
        if params.get('start', None):
            start = dateutil_parse(params.get('start'))

        length = parse_duration(params.get('duration', '1d'))
        end = start + length

        if params.get('end', None):
            end = dateutil_parse(params.get('end'))

        assert end.timestamp() > start.timestamp(), f"end ({end.isoformat()}) should be after start ({start.isoformat()})"

        sunrise = dateutil_parse(params.get('sunrise', '6am')).time()

        photoperiod = parse_duration(params.get("photoperiod", "16h"))

        light_type = params.get('light_type', "none")

        assert light_type in all_light_headers.keys(), f"light_type {light_type} not in list of known lights: {','.join(all_light_headers.keys())}"

        light_h = all_light_headers[light_type]
        light_scaling = [1.0] * len(light_h)
        ls = params.get('light_scaling', "")
        if type(ls) == list:
            light_scaling = light_scaling
        elif type(ls) == str and ls != "":
            light_scaling = [float(x.strip()) for x in ls.split(',')]
        assert len(light_scaling) == len(light_h) or len(light_scaling) == 1, f"light_scaling ({len(light_scaling)}) should be a single value or match the number of wavelengths for the light_type ({len(light_h)})"
        if len(light_scaling) == 1:
            light_scaling = light_scaling * len(light_h)

        temp_model = params.get('temp_model', 'simple')

        assert temp_model in temp_models, f"temp_model not in list of known temperature models: {','.join(temp_models)}"

        humidity_model = params.get('humidity_model', 'simple')

        assert humidity_model in humidity_models, f"humidity_model not in list of known humidity models: {','.join(humidity_models)}"

        interval = parse_duration(params.get('interval', '10m'))
        assert datetime.timedelta(seconds=1) < interval, "this program only supports interval greater than 1s"

        high_temp = float(params.get('high_temp', 25.0))

        assert 0.0 <= high_temp <= 40.0, f"high_temp ({high_temp}) should be within 0-40C"
        low_temp = float(params.get('low_temp', 20.0))
        assert 0.0 <= low_temp <= 40.0, f"low_temp ({low_temp}) should be within 0-40C"

        high_humidity = float(params.get('high_humidity', 55.0))
        assert 0.0 <= high_humidity <= 100.0, f"high_humidity ({high_humidity}) should be within 0-100%"
        low_humidity = float(params.get('low_humidity', 55.0))
        assert 0.0 <= low_humidity <= 100.0, f"low_humidity ({low_humidity}) should be within 0-100%"

        wb, ws = get_excel(light_h)

        current = start

        sunset = (datetime.datetime.combine(datetime.datetime.today(), sunrise) + photoperiod).time()
        temp_spline = None

        if temp_model == "cesaraccio" or temp_model == "complex":
            temp_spline = fit_temp_cesaraccio(min_temp=low_temp, max_temp=high_temp,
                                              sunrise=sunrise, sunset=sunset)

        elif temp_model == "sine":
            temp_spline = fit_temp_sine_fast(min_temp=low_temp, max_temp=high_temp,
                                             sunrise=sunrise)

        while current < end:

            temp = low_temp
            humidity = low_humidity
            channels = [0] * len(light_h)
            if not is_night(current, sunrise=sunrise, photoperiod=photoperiod):
                temp = high_temp
                humidity = high_humidity
                channels = [100] * len(light_h)
                channels = [ch * sf for ch, sf in zip(channels, light_scaling)]

            if temp_spline is not None:
                ch = current.hour + (current.minute / 60.0) + (current.second / 3600) + (current.microsecond / 36e8)
                temp = float(temp_spline(ch))
            if humidity_model == 'allen' or humidity_model == 'complex':
                humidity = relative_humidity(low_temp, temp)

            ws.append([
                current,
                current,
                humidity,
                round(temp, 2),
            ] + channels)

            current += interval
        buf = io.BytesIO()
        wb.save(buf)
        buf.seek(0)

        fname = f"{light_type}_{datetime.datetime.today().date().isoformat()}"
        return func.HttpResponse(
            body=buf.read(),
            status_code=200,
            headers={'Content-Type': "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                     'Content-Disposition': f"attachment; filename={fname}.xlsx",
                     'Access-Control-Expose-Headers': "Content-Disposition",
                     'Access-Control-Allow-Credentials': 'true',
                     'Access-Control-Allow-Origin': "*",
                     'Access-Control-Allow-Headers': "*"
                     }
        )
    except (AssertionError, ParserError) as e:
        return func.HttpResponse(
            body=json.dumps({"error": str(e)}),
            status_code=400,
            headers={'Content-Type': "application/json",
                     'Access-Control-Allow-Credentials': 'true',
                     'Access-Control-Allow-Origin': "*",
                     'Access-Control-Allow-Headers': "*"
                     })
    return func.HttpResponse(status_code=204)
