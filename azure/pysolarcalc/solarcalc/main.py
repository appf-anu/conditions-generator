import azure.functions as func
import json
# import logging
import datetime
import io
import re
import numpy as np
from solarcalc.light_sim import LightSim
from solarcalc.spectopt import Light, Spectrum

from dateutil.parser import parse as dateutil_parse
from dateutil.parser import ParserError
from openpyxl import Workbook

import os
import warnings
warnings.filterwarnings("ignore")

all_light_headers = {
    "fytopanel": [
        "coolwhite",  # "channel-1",  # white
        "blue",       # "channel-2",  # blue
        "green",      # "channel-3",  # green
        "red",        # "channel-4",  # red
        "deepred",    # "channel-5",  #  deepred
        "farred",     # "channel-6",  # far red
        "infrared",   # "channel-7",  # infrared
        "cyan",       # "channel-8"   # NA
    ],
    "capsule": [
        "coolwhite",
        "deepred",
        "farred",
    ],
    "capsule_fancy": [
        "coolwhite",
        "deepred",
        "farred",
        "royalblue",
        "blue",
        "cyan"
    ],
    "old_fytopanel": [
        "channel-1",  # white
        "channel-2",  # blue
        "channel-3",  # green
        "channel-4",  # nearest red
        "channel-5",  # near red
        "channel-6",  # far red
        "channel-7",  # infra red
        "channel-8"   # unknown, further infra red?
    ],
    "heliospectra_s7": [
        "400nm",  # channel-1",  # 400nm
        "420nm",  # channel-2",  # 420nm
        "450nm",  # channel-3",  # 450nm
        "530nm",  # channel-4",  # 530nm
        "630nm",  # channel-5",  # 630nm
        "660nm",  # channel-6",  # 660nm
        "735nm",  # channel-7"   # 735nm
    ],
    "heliospectra_s10": [
        "370nm",  # channel-1",  # 370nm
        "400nm",  # channel-2",  # 400nm
        "420nm",  # channel-3",  # 420nm
        "450nm",  # channel-4",  # 450nm
        "530nm",  # channel-5",  # 530nm
        "620nm",  # channel-6",  # 620nm
        "660nm",  # channel-7",  # 660nm
        "735nm",  # channel-8",  # 735nm
        "850nm",  # channel-9",  # 850nm
        "6500k",  # channel-10"  # 6500k
    ],
    "heliospectra_dyna": [
        "380nm",  # channel-1",  # 370nm
        "400nm",  # channel-2",  # 400nm
        "420nm",  # channel-3",  # 420nm
        "450nm",  # channel-4",  # 450nm
        "530nm",  # channel-5",  # 530nm
        "620nm",  # channel-6",  # 620nm
        "660nm",  # channel-7",  # 660nm
        "735nm",  # channel-8",  # 735nm
        "5700k",  # channel-10"  # 6500k
    ],
    "conviron": [
        "light1",
        "light2"
    ]
}

light_files = {
    'fytopanel': "pgf_psi_wm2.csv",
    'capsule': "growtainer-white-high-light-wm2.csv",
    'capsule_fancy': "growtainer-multispectral-wm2.csv",
    'heliospectra_s7': "pgf_heliospectra_s7_est_wm2.csv",
    'heliospectra_s10': "pgf_heliospectra_s10_wm2_par_only.csv",
    'heliospectra_dyna': "pgf_heliospectra_dyna_wm2.csv",
    'conviron': "cool_white_fluoro.csv",
}

_relative_path = os.path.dirname(__file__)
lights = {
    'fytopanel': Light(os.path.join(_relative_path, "data/spectra/pgf_psi_wm2.csv")),
    'capsule': Light(os.path.join(_relative_path, "data/spectra/growtainer-white-high-light-wm2.csv")),
    'capsule_fancy': Light(os.path.join(_relative_path, "data/spectra/growtainer-multispectral-wm2.csv")),
    'heliospectra_s7': Light(os.path.join(_relative_path, "data/spectra/pgf_heliospectra_s7_est_wm2.csv")),
    'heliospectra_s10': Light(os.path.join(_relative_path, "data/spectra/pgf_heliospectra_s10_wm2_par_only.csv")),
    'heliospectra_dyna': Light(os.path.join(_relative_path, "data/spectra/pgf_heliospectra_dyna_wm2.csv")),
    'conviron': Light(os.path.join(_relative_path, "data/spectra/cool_white_fluoro.csv")),
}


def clamp(n, minimum=0, maximum=100):
    n = max(n, minimum)
    n = min(n, maximum)
    return n


def get_excel(light_headers):
    """
    gets an in-memory excel workbook and sheet with the title timepoints
    dateteime fields (the first two fields), require an style of width: 19
    """
    wb = Workbook(write_only=True)
    ws = wb.create_sheet(title="timepoints")
    ws2 = wb.create_sheet(title="solar_spectra")
    # datetime has 19 characters
    ws.column_dimensions['A'].width = 19
    ws.column_dimensions['B'].width = 19
    ws.append(["datetime", "datetime-sim", "humidity", "temperature", ] + light_headers)
    ws2.append(["datetime", "datetime-sim", "total_solar"])
    ws2.column_dimensions['A'].width = 19
    ws2.column_dimensions['B'].width = 19
    return wb, ws, ws2


# regex to match different duration keys to named groups
duration_re = re.compile(r'((?P<weeks>\d+?)w)?((?P<days>\d+?)d)?((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')


def parse_duration(time_str):
    """
    parse a duration string into a timedelta using duration_re
    """
    parts = duration_re.match(time_str)
    assert parts is not None, f"could not parse duration from '{time_str}'. examples of valid duration: '8h', '2d8h5m20s', '2m4s'"

    time_params = {name: int(param) for name, param in parts.groupdict().items() if param}
    return datetime.timedelta(**time_params)


def main(req: func.HttpRequest) -> func.HttpResponse:
    try:
        params = req.params.copy()
        if 'application/json' in req.headers.get('Content-Type', req.headers.get('content-type', "")):
            params.update(req.get_json())

        if req.form is not None and len(req.form):
            params.update(req.form)

        params = {k: v for k, v in params.items() if v != ""}

        start = datetime.datetime.now().replace(
            microsecond=0,
            second=0,
            minute=0,
            hour=0
        )
        if params.get('start', None):
            start = dateutil_parse(params.get('start'))

        length = parse_duration(params.get('duration', '1d'))
        end = start + length

        latitude = float(params.get('latitude', -9999.0))
        longitude = float(params.get('longitude', -9999.0))

        assert latitude != -9999.0 and longitude != -9999.0, "latitude and longitude cannot be empty"

        elev = float(params.get('elevation', 0))

        if params.get('end', None):
            end = dateutil_parse(params.get('end'))

        assert end.timestamp() > start.timestamp(), f"end ({end.isoformat()}) should be after start ({start.isoformat()})"

        assert (end - start) <= datetime.timedelta(days=14), "time range is greater than 14d, please select a shorter time range"

        if params.get('actual_start', None):
            chamber_start = dateutil_parse(params.get('actual_start'))
        else:
            chamber_start = start

        light_type = params.get('light_type', "none")

        assert light_type in all_light_headers.keys(), f"light_type {light_type} not in list of known lights: {','.join(all_light_headers.keys())}"

        light_h = all_light_headers[light_type]

        scale_factor = float(params.get('scale_factor', 0.5))

        interval = parse_duration(params.get('interval', '10m'))
        assert datetime.timedelta(minutes=1) < interval, "this program only supports interval greater than 1 minute"

        wavelengths = np.arange(350, 800, 5, dtype=float)

        sim = LightSim(start, end, latitude, longitude, elev, wavelengths=wavelengths)

        light = lights[light_type]
        wb, ws, ws2 = get_excel(light_h)
        date_offset = chamber_start - start
        current = start
        while current < end:
            reald = current
            reald = current + date_offset
            doyf = current.timetuple().tm_yday + current.minute / 1440.0 + current.hour / 24.0
            theory = sim.combined_spline(doyf)
            temp, rh, totsrad, *spectrum = theory
            spectrum = Spectrum(wavelengths=wavelengths,
                                values=np.array(spectrum) / 1000 * scale_factor,
                                watts=True)

            out = [clamp(x * 100) for x in list(light.optimise_settings(spectrum, watts=False))]
            if light_type == 'conviron':
                out = out + out

            ws.append([
                reald,
                current,
                rh,
                temp
            ] + out)

            ws2.append([
                reald,
                current,
                totsrad
            ] + list(spectrum.photons().interpolated()(wavelengths) * 1e9))
            current += interval

        buf = io.BytesIO()
        wb.save(buf)
        buf.seek(0)

        fname = f"{light_type}_{datetime.datetime.today().date().isoformat()}"
        return func.HttpResponse(
            body=buf.read(),
            status_code=200,
            headers={'Content-Type': "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                     'Content-Disposition': f"attachment; filename={fname}.xlsx",
                     'Access-Control-Expose-Headers': "Content-Disposition",
                     'Access-Control-Allow-Credentials': 'true',
                     'Access-Control-Allow-Origin': "*",
                     'Access-Control-Allow-Headers': "*"
                     }
        )
    except (AssertionError, ParserError) as e:
        return func.HttpResponse(
            body=json.dumps({"error": str(e)}),
            status_code=400,
            headers={'Content-Type': "application/json",
                     'Access-Control-Allow-Credentials': 'true',
                     'Access-Control-Allow-Origin': "*",
                     'Access-Control-Allow-Headers': "*"
                     })
    return func.HttpResponse(status_code=204)
